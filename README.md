# Erato #

NodeJS/Express + Spotify API application that recommends artists filtered through an inputted obscurity value. Users can then login through Spotify to generate a playlist based on recommendations.